This repository contains a beamer theme plus an example build flow.

The build flow uses:
- pdflatex
- gnumake for automation
- inkscapeto generate pdf from svg files
- libreoffice to generate pdf from odg files
- xfig to generate pdf from fig files


**If you just want to use the theme and not interested in the rest, just copy:**

- the sty theme file `beamerthemetptng.sty`
- the pdf logos files

from the [`texinputs`](texinputs) directory.
