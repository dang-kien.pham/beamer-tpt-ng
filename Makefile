#
# Use this Makefile with GNU make.
#
TOP ?= example-presentation.pdf
TOP_NAME = $(patsubst %.pdf,%,$(TOP))

STYS       = $(wildcard texinputs/*.sty)

FIGS       = $(wildcard figs/*.fig)
FIGS_PDF   = $(patsubst %.fig,%.pdftex,$(FIGS))
FIGS_PDF_T = $(patsubst %.fig,%.pdftex_t,$(FIGS))

SVGS       = $(wildcard svgs/*.svg)
SVGS_PDF   = $(patsubst %.svg,%.pdf,$(SVGS))

ODGS       = $(wildcard odgs/*.odg)
ODGS_PDF   = $(patsubst %.odg,%.pdf,$(ODGS))

NEEDED = $(FIGS_PDF) $(FIGS_PDF_T) $(SVGS_PDF) $(ODGS_PDF)

export TEXINPUTS := ./texinputs/:$(TEXINPUTS)

#.SECONDARY: $(FIGS_PDF)

.PHONY: all clean dep preview

all: $(TOP)

$(TOP) : dep

dep: $(NEEDED) $(NEEDED_DOC)

%.pdf:%.tex $(STYS)
	@echo "LaTeX search path $(TEXINPUTS)"
	@latexmk -pdf $<

preview: dep
	pdflatex  '$(PREVIEW_OPTS) \input{$(TOP_NAME).tex}'

clean: .latexmkrc
	latexmk -c $(TOP_NAME)

cleaner: .latexmkrc
	@echo Cleaning $(TOP) and $(NEEDED)
	@latexmk -C $(TOP_NAME)
	@rm -f $(NEEDED)


%.pdftex:%.fig
	fig2dev -L pdftex $< $@
%.pdftex_t:%.pdftex
	fig2dev -L pdftex_t -p $< $(patsubst %.pdftex_t,%.fig,$@) $@

%.swf:%.pdf
	pdf2swf $< && chmod -x $@

# Get Inkscape version
INKSCAPEVERSION = $(shell inkscape --version | cut -f2 -d" ")
# inkscapever -> x.yy.z
INKSCAPEVERSION_MAJOR = $(shell echo $(INKSCAPEVERSION) | cut -f1 -d.)

%.pdf:%.svg
	if [ "$(INKSCAPEVERSION_MAJOR)" = "0" ]; \
	then \
		inkscape -f $< --export-pdf=$@ ; \
	else \
		inkscape --export-type=pdf -o $@ $< ; \
	fi

%.pdf:%.odg
	libreoffice --headless --convert-to pdf $< --outdir odgs
	pdfcrop --margins 1 $@ $@

initlatexmkrc: .latexmkrc
	@echo 'setting up latexmk for extended cleaning'
	@echo '$$clean_ext = "bbl nav out snm";' > .latexmkrc

.latexmkrc:
	touch .latexmkrc